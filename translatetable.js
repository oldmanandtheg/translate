function translateWords (words,callback) {
	with_options(function(items) {
		var uri = "https://translate.yandex.net/api/v1.5/tr.json/translate?"
		uri = uri+"key="+items.yandex_key+"&lang=en-"+items.yandex_target;
		for (index in words) {
			word = words[index];
			forms = nlp.verb(words[index]).conjugate();
			if (forms !== undefined && forms.infinitive !== undefined)
				word = forms.infinitive;
			uri += "&text="+word;
		}
		$.ajax({
			dataType: "json",
			url: uri,
			success: function(result) {
				if (result !== undefined && result.text !== undefined) {
					mapping = {};
					for (i in result.text) {
						if (words[i] !== result.text[i]) mapping[words[i]] = result.text[i];
					}
					callback(mapping);
				}
				// TODO: what if this fails?
			}
		});
	})
}

function getWordFreqs(texts) {
	var freqs = {};
	$.each(texts, function(index,text) {
		words = getwords(text);
	    $.each(words, function(index, word) {
	        if (!(word in freqs)) freqs[word] = 0;
	        freqs[word]++;
	    });
	});
	return freqs;
}

//Get words in the text content of a given element that fit certain POS's, process, save for translation. 
function getwords(text,freqs){
    var words = new Lexer().lex(text);
    var tagged = new POSTagger().tag(words);
    var acceptabletags = {"VB":1,"VBP":1,"VBZ":1};
    var words = tagged
        .filter(function(item){return item[1] in acceptabletags})
        .map(function(item){
            var word = item[0].replace(/[^a-zA-Z-]/g,"");
            if (word !== "I") word = word.toLowerCase();
            return word;
        });
    return words;
}