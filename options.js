// the languages first author believes are used by yandex
// second author found a service for this, but doesn't care
function get_language_options() {
  var codes = {
    "cs": "Czech (čeština)",
    "de": "German (Deutsch)",
    "en": "English",
    "es": "Spanish (español)",
    "fr": "French (français)",
    "it": "Italian (italiano)",
    "pl": "Polish (język polski)",
    "ro": "Romanian (limba română)",
    "ru": "Russian (Русский)",
    "sr": "Serbian (српски језик)",
    "tr": "Turkish (Türkçe)",
    "uk": "Ukranian (українська мова)",
  };
  return codes;
}

function with_options(callback) {
  chrome.storage.sync.get({
    yandex_target: 'en',
    yandex_key: ""
  }, callback);
}