function save_options() {
  var yandex_target = document.getElementById('yandex_target').value;
  var yandex_key = document.getElementById('yandex_key').value;
  chrome.storage.sync.set({
    yandex_target: yandex_target,
    yandex_key: yandex_key
  }, function() {
    window.close();
  });
}

function load_options(items) {
  $('#yandex_target')
    .find('option')
    .remove()
    .end();
  selectValues = get_language_options();
  $.each(selectValues, function(key, value) {   
     $('#yandex_target')
          .append($('<option>', { value : key })
          .text(value)); 
  });
  $('#yandex_key').val(items.yandex_key);
  $('#yandex_target').val(items.yandex_target);
  $('#save').click(save_options);
}

document.addEventListener('DOMContentLoaded', function() { with_options(load_options); });