// adapted from highlighting fn: http://www.gotoquiz.com/web-coding/programming/javascript/highlight-words-in-text-with-jquery/ 
$.fn.tooltip = function (str, trans) {
    var regex = new RegExp("(\\W|^)("+str+")(\\W)", "gi");
    return this.each(function () {
        $(this).contents().filter(function() {
            return this.nodeType == 3 && regex.test(this.nodeValue);
        }).replaceWith(function() {
            return (this.nodeValue || "").replace(regex, function(match) {
                var parts = regex.exec(match);
                if (parts && parts.length == 4){
                    return parts[1]+"<a href=\"https://en.wiktionary.org/w/index.php?search="+encodeURIComponent(trans)+"\" title=\"" + parts[2] + "\">" + trans + "</a>" + parts[3];
                }
                return match;
            });
        });
    });
}

//Generate inner HTML for table with data in datatable and headers in headerlist
function tableHTML(datatable){
    var html="";
    for (var r = 0; r<datatable.length; r++){
        html += "<tr>";
        for (var d = 0; d<datatable[r].length; d++){
            html += "<td>" + datatable[r][d] + "</td>";
        }
        html += "</tr>";
    }
    return html;
}

//Listens for translation request: if new or if parameters have changed, regenerates table html by running runmain. Else, returns previous html. 
var html;
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if (request.type == "translate"){
        if (html !== undefined) {
            sendResponse({tablehtml: html});
            return false;
        }
        var pars = $("p");
        var texts = $("p").map(function (index, value) {return value.textContent});
        var freqs = getWordFreqs(texts);
        var sortedwords = Object.keys(freqs)
            .sort(function (a,b){
                return (freqs[b])-(freqs[a]);
            });
        // TODO: translate only new words?
        translateWords(sortedwords,function(translations){
            table = sortedwords
                .filter(function(word) { return word in translations })
                .map(function(word) {return [word, freqs[word], translations[word]]});
            $.each(translations, function(key, value) {
                pars.tooltip(key, value);
            });
            html = tableHTML(table);
            sendResponse({tablehtml: html});
        });
        return true;
    }
});